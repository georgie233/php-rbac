<?php

namespace Georgie\PhpRbac\validate;


use think\Validate;

class Permission extends Validate
{
    protected $failException = true;

    protected $rule = [
        'name' => 'require|max:50',
        'type' => 'require|number',
        'category_id' => 'require|number',
        'path' => 'require|max:100',
    ];

    protected $message = [
        'name.require' => '权限名不能为空',
        'name.max' => '权限名不能长于50个字符',

        'type.require' => '权限类型不能为空',
        'type.number' => '权限类型有误',

        'category_id.require' => '权限分组不能为空',
        'category_id.number' => '权限分组类型有误',

        'path.number' => '权限路径不能为空',
        'path.max' => '权限路径不能长于1000个字符',
    ];
}