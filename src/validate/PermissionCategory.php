<?php

namespace Georgie\PhpRbac\validate;


use think\Validate;

class PermissionCategory extends Validate
{
    protected $failException = true;

    protected $rule = [
        'name' => 'require|max:50',
    ];

    protected $message = [
        'name.require' => '分组名不能为空',
        'name.max' => '分组名不能长于50个字符',
    ];
}