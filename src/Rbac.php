<?php

namespace Georgie\PhpRbac;

use Georgie\PhpRbac\model\Organize;
use Georgie\PhpRbac\model\Permission;
use Georgie\PhpRbac\model\PermissionCategory;
use Georgie\PhpRbac\model\Position;
use Georgie\PhpRbac\model\Role;
use Georgie\PhpRbac\model\User;
use Georgie\PhpRbac\model\UserGroup;

interface RbacInterface
{
    public function createTable(); //创建依赖表
    public function coverCreateTable(); //替换创建依赖表
    public function createPermissionCategory(); //创建权限分组
    public function editPermissionCategory(); //修改权限分组
    public function removePermissionCategory(); //删除权限分组
    public function getPermissionCategory(); //获取权限分组
    public function createPermission(); //创建权限
    public function editPermission(); //修改权限
    public function removePermission(); //删除权限
    public function getPermission(); //获取权限
    public function createOrganize(); //创建组织
    public function editOrganize(); //编辑组织
    public function removeOrganize(); //删除组织
    public function getOrganize(); //获取组织
    public function createPosition();//创建职位
    public function editPosition();//修改职位
    public function removePosition();//删除职位
    public function getPosition();//获取职位
    public function createUserGroup();//创建角色组
    public function editUserGroup();//创建角色组
    public function removeUserGroup();//创建角色组
    public function getUserGroup();//创建角色组
    public function createUser();//创建用户
    public function editUser();//创建用户
    public function removeUser();//创建用户
    public function getUser();//创建用户
    public function createRole();//创建角色
    public function editRole();//创建角色
    public function removeRole();//创建角色
    public function getRole();//创建角色
}

class Rbac implements RbacInterface
{
    public function createTable($prefix = '', $db = '')
    {
        (new CreateTable())->create($prefix, $db, false);
    }

    public function coverCreateTable($prefix = '', $db = '')
    {
        (new CreateTable())->create($prefix, $db, true);
    }

    public function createPermissionCategory(array $data = [])
    {
        return (new PermissionCategory())->data($data)->create();
    }

    public function editPermissionCategory(array $data = [])
    {
        return (new PermissionCategory())->data($data)->edit();
    }

    public function removePermissionCategory($id = 0)
    {
        return (new PermissionCategory())->del($id);
    }

    public function getPermissionCategory(array $condition = ['status' => 1])
    {
        return (new PermissionCategory())->get($condition) ?? [];
    }

    //权限

    public function createPermission(array $data = [])
    {
        return (new Permission())->data($data)->create();
    }

    public function editPermission(array $data = [])
    {
        return (new Permission())->data($data)->edit();
    }

    public function removePermission($id = 0)
    {
        return (new Permission())->del($id);
    }

    public function getPermission(array $condition = ['status' => 1])
    {
        return (new Permission())->get($condition) ?? [];
    }

    //组织

    public function createOrganize(array $data = [])
    {
        return (new Organize())->data($data)->create();
    }

    public function editOrganize(array $data = [])
    {
        return (new Organize())->data($data)->edit();
    }

    public function removeOrganize($id = 0)
    {
        return (new Organize())->del($id);
    }

    public function getOrganize(array $condition = ['status' => 1])
    {
        return (new Organize())->get($condition) ?? [];
    }

    public function createPosition(array $data = [])
    {
        return (new Position())->data($data)->create();
    }

    public function editPosition(array $data = [])
    {
        return (new Position())->data($data)->edit();
    }

    public function removePosition($id = 0)
    {
        return (new Position())->del($id);
    }

    public function getPosition(array $condition = ['status' => 1])
    {
        return (new Position())->get($condition) ?? [];
    }

    public function createUser(array $data = [])
    {
        return (new User())->data($data)->create();
    }

    public function editUser(array $data = [])
    {
        return (new User())->data($data)->edit();
    }

    public function removeUser($id = 0)
    {
        return (new User())->del($id);
    }

    public function getUser(array $condition = ['status' => 1])
    {
        return (new User())->get($condition) ?? [];
    }

    public function createRole(array $data = [])
    {
        return (new Role())->data($data)->create();
    }

    public function editRole(array $data = [])
    {
        return (new Role())->data($data)->edit();
    }

    public function removeRole($id = 0)
    {
        return (new Role())->del($id);
    }

    public function getRole(array $condition = ['status' => 1])
    {
        return (new Role())->get($condition) ?? [];
    }

    public function createUserGroup(array $data = [])
    {
        return (new UserGroup())->data($data)->create();
    }

    public function editUserGroup(array $data = [])
    {
        return (new UserGroup())->data($data)->edit();
    }

    public function removeUserGroup($id = 0)
    {
        return (new UserGroup())->del($id);
    }

    public function getUserGroup(array $condition = ['status' => 1])
    {
        return (new UserGroup())->get($condition) ?? [];
    }
}