<?php

namespace Georgie\PhpRbac\model;

class Organize extends Base
{
    public $tableName = 'organize';

    public function del($id = 0)
    {
        $id_ = $id ?? ($this->data_['id'] ?? 0);
        if (!$id_) throw new \Exception('请传入ID');

        $this->db()->where('id', $this->data_['id'])->delete();

        //代办：排除存在关联数据的

        return true;
    }
}