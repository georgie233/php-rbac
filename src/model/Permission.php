<?php

namespace Georgie\PhpRbac\model;

class Permission extends Base
{
    protected $tableName = 'permission';


    /**
     * 创建权限分组
     * @param $data array 创建的数据
     * @return bool
     * @author Georgie 227040015@qq.com
     */
    public function create($data = [])
    {
        if (!empty($data)) {
            $this->data($data);
        }

        $validate = new \Georgie\PhpRbac\validate\Permission();
        $validate->check($this->data_);

        $this->data_['create_time'] = time();
        $this->db()->insert($this->data_);

        return true;
    }

    /**
     * 修改权限
     * @param $data array 修改的数据
     * @return bool
     * @author Georgie 227040015@qq.com
     */
    public function edit($data = [])
    {
        if (!empty($data)) {
            $this->data($data);
        }

        $validate = new \Georgie\PhpRbac\validate\Permission();
        $validate->check($this->data_);

        if (!$this->data_['id'])
            throw new \Exception('请传入ID');

        $this->db()->where('id', $this->data_['id'])->update($this->data_);

        return true;
    }


    /**
     * 删除权限
     * @param $id integer 删除的ID
     * @return bool
     * @author Georgie 227040015@qq.com
     */
    public function del($id = 0)
    {
        $this->data(['id' => $id ?? ($this->data_['id'] ?? 0)]);
        if (!$this->data_['id']) throw new \Exception('请传入ID');

        $this->db()->where('id', $this->data_['id'])->delete();

        return true;
    }

    /**
     * 根据条件获取数据
     * @param $condition array 筛选条件
     * @return array
     * @author Georgie 227040015@qq.com
     */
    public function get(array $condition = ['status' => 1])
    {
        $orm = $this->db()->where($condition);

        if (isset($condition['id']))
            return $orm->find();
        else
            return $orm->select();
    }

}